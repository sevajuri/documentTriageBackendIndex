sys.path.insert(0, '../')
from _config import *

class analizeArticle():
    
    def __init__(self, article):
        self.article = article

    def printError(self):
        pass

    def logError(self):
        pass
    
    def normalizeJournal(self, articleTitle):
        pass
    
    def annotateArticle(self, pmid, pickleFile, informationSource):
        pass
    
    def normalizeKeys(self, article):
        pass
    
    def decodeKeys(self, article):
        pass
    
    def classifyArticle(self, article):
        pass