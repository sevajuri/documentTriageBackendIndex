# -*- coding: utf-8 -*-
import scrapy
import sys
from scrapy.spiders import Spider
from scrapy.selector import Selector
from asco.items import AscoItem
from bs4 import BeautifulSoup
import requests
import sys
from os.path import isfile
import cPickle as pickle
import re

sys.path.insert(0, '../')
from _config import *

class AscoSpider(scrapy.Spider):
    
    alreadyDownDict = {}
    if isfile(alreadyDownASCO):
        alreadyDownDict = pickle.load(open(alreadyDownASCO, 'rb'))        
    
    name = "Asco"
    allowed_domains = ["asco.org"]
    r = requests.get('http://meetinglibrary.asco.org/abstractbysubcategory/')
    print r.text
    soup = BeautifulSoup(r.text,"lxml")
    mydivs = soup.findAll("div", { "class" : "view-content" })
    print mydivs
    raw_input('prompt')
    links = BeautifulSoup(str(mydivs[0]), "lxml").findAll('a')
    meetings = ['http://meetinglibrary.asco.org'+item.get('href') for item in links] #ALL MEETINGS FROM ASCO
    start_urls = []

    for meet in meetings:

        r = requests.get(meet)
        soup = BeautifulSoup(r.text,"lxml")
        articleContent = soup.findAll("div", { "class" : "view-content" })
        try:
            if meet not in alreadyDownDict:
                print "Preparing :\t{}".format(meet)
                start_urls.extend(['http://meetinglibrary.asco.org'+item.get('href') for item in BeautifulSoup(str(articleContent[0]), "lxml").findAll('a') if 'http' not in item.get('href')])
                alreadyDownDict[meet]=True
            else:
                print "Skipped as already downloaded:\t{}".format(meet)

        except:
            print "Skipped:\t{}".format(meet)

    pickle.dump(alreadyDownDict,open(alreadyDownASCO, 'wb'))

    #===========================================================================
    # //*[@id="region-content"] -> LIST OF ARTICLES ON YEARS ANNUAL MEETING PAGE
    # //*[@id="block-system-main"] -> MAIN CONTENT
    # //article[contains(@id,'node-abstract')]/div[1]/div[1]/div/div/div/div[2] -> title
    # //article[contains(@id,'node-abstract')]/div[1]/div[2]/div[2]/div/div/div/a -> subcategory, hyperlink
    # //article[contains(@id,'node-abstract')]/div[1]/div[3]/div[2]/div/div/div -> category
    # //article[contains(@id,'node-abstract')]/div[1]/div[4]/div[2]/div/div/div/a -> meeting, hyperlink
    # //article[contains(@id,'node-abstract')]/div[1]/div[6]/div[2]/div -> abstract number
    # //article[contains(@id,'node-abstract')]/div[1]/div[7]/div[2]/div -> citation 
    # //article[contains(@id,'node-abstract')]//div[1]/div[8]/div[2]/div[1] -> authors
    # //article[contains(@id,'node-abstract')]/div[1]/div[9]/div/div/div[1] -> abstract (extended form with some other stuff there as well)
    #===========================================================================

#    def __init__(self):
 #       super(MyClass, self).__init__()
  #      logging_to_file(self.name)
    
    def cleanString(self, text):
        pass

    def parse(self, response):
        
        
        for sel in response.xpath("//article[contains(@id,'node-abstract')]"):
            item = AscoItem()

            #category
            #item['category'] = ''.join( sel.xpath('div[1]/div[3]/div[2]/div/div/div/text()').extract() ).strip()
            
            #===================================================================
            # #subcategory
            # if ''.join( sel.xpath('div[1]/div[2]/div[2]/div/div/div/a/text()').extract() ).strip() == "":
            #     item['subcategory'] = ''.join( sel.xpath('div[1]/div[3]/div[2]/div/div/div/a/text()').extract() ).strip()
            # else:
            #     item['subcategory'] = ''.join( sel.xpath('div[1]/div[2]/div[2]/div/div/div/a/text()').extract() ).strip()
            #===================================================================
    
            #===================================================================
            # #subcategoryNr
            # if ''.join( sel.xpath('div[1]/div[2]/div[2]/div/div/div/a/@href').extract() ).strip() == "":
            #     item['subcategoryNr'] = ''.join( sel.xpath('div[1]/div[3]/div[2]/div/div/div/a/@href').extract() ).strip()
            # else:        
            #     item['subcategoryNr'] = ''.join( sel.xpath('div[1]/div[2]/div[2]/div/div/div/a/@href').extract() ).strip()
            #===================================================================
                
            #item['sessionTypeTitle'] = ''.join( sel.xpath('div[1]/div[5]/div[2]/div/div/div/text()').extract() ).strip()
            
            #===================================================================
            # #abstract number
            # if ''.join(  sel.xpath('div[1]/div[6]/div[2]/div/p/text()').extract() ).strip() == "":
            #     item['AbstractNumber'] = ''.join( sel.xpath('div[1]/div[6]/div[2]/div/text()').extract() ).strip()
            # else:            
            #     item['AbstractNumber'] = ''.join(  sel.xpath('div[1]/div[6]/div[2]/div/p/text()').extract() ).strip()
            #===================================================================
            
            if ''.join( sel.xpath('div[1]/div[1]/div/div/div/div[2]/h3[1]/text()').extract() ).strip() == "":
                item['title'] = ''.join( sel.xpath('div[1]/div[2]/div/div/div/div[2]/h3[1]//text()').extract() ).replace('\r', '').replace('\n', '').strip()
            else:
                item['title'] = ''.join( sel.xpath('div[1]/div[1]/div/div/div/div[2]/h3[1]//text()').extract() ).replace('\r', '').replace('\n', '').strip()

            item['authors'] = ''.join( sel.xpath('div[1]/div[9]/div[2]/div/text()').extract() ).replace('\r', '').replace('\n', '').strip()
            item['journal'] = ''.join(  sel.xpath('div[1]/div[8]/div[2]/div[1]/text()').extract() ).strip()
            
	    #===================================================================
            #abstract
	    #===================================================================
            #//*[@id="node-abstract-2601741"]/div[1]/div[10]/div/div/div[1]/div/p[2]
            if ''.join( sel.xpath('div[1]/div[9]/div/div/div[1]/div/*[not(ancestor-or-self::style) or not(descendant-or-self::style)]//text()').extract() ).replace('\r', '').replace('\n', '').replace('\t', '').replace('Abstract Disclosures','').strip() == '':
                item['abstract'] = ''.join( sel.xpath('div[1]/div[10]/div/div/div[1]/div/*[not(ancestor-or-self::style) or not(descendant-or-self::style)]//text()').extract() ).replace('\r', '').replace('\n', '').replace('\t', '').replace('Abstract Disclosures','').replace("`",'').replace('"', '').strip()
            else:
                item['abstract'] = ''.join( sel.xpath('div[1]/div[9]/div/div/div[1]/div/*[not(ancestor-or-self::style) or not(descendant-or-self::style)]//text()').extract() ).replace('\r', '').replace('\n', '').replace('\t', '').replace('Abstract Disclosures','').replace("`",'').replace('"', '').strip()
            
            #===================================================================
            # rest
            #===================================================================
            item['meeting'] = ''.join( sel.xpath('div[1]/div[4]/div[2]/div/div/div/a/text()').extract() ).replace('\r', '').replace('\n', '').strip()
            item['pubdate']=     re.search('\d{4}', item['meeting']).group(0)
            item['pmid']=response.xpath('//h1[@id="page-title"]/text()').extract_first().replace('\r', '').replace('\n', '').strip()            
            item['articleLink']='http://meetinglibrary.asco.org/content/'+item['pmid'].replace('\r', '').replace('\n', '').strip()

            yield item
            #===================================================================
            # raw_input('prompt')
            #===================================================================
