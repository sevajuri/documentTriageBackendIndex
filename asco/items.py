# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class AscoItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    pmid = scrapy.Field()
    title = scrapy.Field()
    category = scrapy.Field()
    subcategory = scrapy.Field()
    subcategoryNr = scrapy.Field()
    meeting = scrapy.Field()
    sessionTypeTitle = scrapy.Field()
    AbstractNumber = scrapy.Field()
    journal = scrapy.Field()
    authors = scrapy.Field()
    abstract = scrapy.Field()
    articleLink = scrapy.Field()
    pubdate = scrapy.Field()
    
    #===========================================================================
    # SOLR_FIELDS =['nlm_unique_id', 'mesh_terms',  'pubdate',  'abstract',  'cancerType',  'pmc',  'mutation_normalizedValue',  
    #                            'confidence_which', 'title',  'confidence_is',   'articleLink',  'pmid',  'journal',  'gene_name'] 
    #===========================================================================