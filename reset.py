import cPickle as pickle
import sys
from os import listdir
from os.path import isfile, join

sys.path.insert(0, '../')
from _config import *

inputFiles = []
inputFiles.extend([files for files in listdir(PubMed_DLPath) if isfile(join(PubMed_DLPath, files))])
alreadyDownDict= {}
alreadyDownDict= {item:True for item in inputFiles}
pickle.dump(alreadyDownDict, open(alreadyDownPubMed,'wb'))

inputFiles = []
inputFiles.extend([files for files in listdir(PMC_DLPath) if isfile(join(PMC_DLPath, files))])
alreadyDownDict= {}
alreadyDownDict= {item.split('.')[0]:True for item in inputFiles}
pickle.dump(alreadyDownDict, open(alreadyDownPMC,'wb'))