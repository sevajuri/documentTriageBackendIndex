import pubmed_parser as pp
import json, jsonpickle
from os import listdir
from os.path import isfile, join, splitext
import progressbar
import io, json
import tempfile
import traceback
import gzip
from multiprocessing import Pool, Queue, Process, Manager, current_process, active_children # use threads
import tqdm
import sys
from docutils.nodes import raw
import cPickle as pickle

files = '/home/docClass/files/'
PubMed_DLPath = files + 'pubmed/'
PMC_DLPath = files + 'pmc/'
journalsPickle = pickle.load(open('/home/docClass/resources/J_Medline.p','rb'))

def normalizeJournalTitle(article):
    #===========================================================================
    # MAPPINGS
    #===========================================================================
    
    #journalsPickleFields = ['MedAbbr','IsoAbbr']
    #articleFields = ['publisher_id',,,,,]
    journal = ''
    
    fieldMappings = {
            'issn_epub':['ISSN (Online)'],
            'issn_ppub':['ISSN (Print)'],
            'issn_linking':[
                                    'ISSN (Online)',
                                    'ISSN (Print)'
                                    ],
            'nlm_unique_id':['NlmId'],
            'medline_ta':['MedAbbr','IsoAbbr']
        }

    for item in fieldMappings:
        pass

    print 'Original journal name\t' + article['journal']
    print 'Normalized journal name\t'+journal
    return journal

def getArticle(file):
    #===========================================================================
    # file -> path to downloaded PubMed/PMC files
    #===========================================================================

    _, extension = splitext(file)
    items=None

    try:
        if extension == '.gz':
            content=gzip.open(file)
            items = pp.parse_medline_xml(content)
        elif extension == '.nxml':
            content=open(file,'rb')
            items = pp.parse_pubmed_xml(content)
            if isinstance(items, dict):
                items=[items]
        items = [item for item in items if 'delete' not in item or not item['delete']]
    except Exception as e:
        print e
    return items

def processFiles(fileName):   

    articles = getArticle(fileName)
    tmpList_json = []
    textFields = ['publisher_id','journal','medline_ta']

    #print 'Before prepareing: {}'.format(len(articles))
    if articles is not None:
        for article in articles:
            if article['journal'].strip() not in journalsPickle:
                normalizeJournalTitle(article)
                print '-------------------'
                raw_input('prompt')
    return tmpList_json

def analyse(type=0):
    
    print "Prepraing Input Files"
    inputFiles = []
    inputFiles.extend([PubMed_DLPath+files for files in listdir(PubMed_DLPath) if isfile(join(PubMed_DLPath, files))])
    inputFiles.extend([PMC_DLPath+files for files in listdir(PMC_DLPath) if isfile(join(PMC_DLPath, files))])
    bar = progressbar.ProgressBar(max_value=len(inputFiles))
    print "Done prepraing Input Files"    
    
    if type==0:
        for item in bar(inputFiles):
            x = processFiles(item)

    #===============================================================================
    # PARALLEL
    #===============================================================================
    if type==1:    
    
        p = Pool(processes=22)
        pool_outputs =[]
    
        try: 
            for res in tqdm.tqdm(p.imap_unordered(processFiles, inputFiles), total=len(inputFiles)):
                if len(res) > 0:
                    pool_outputs.extend(res)
                    if pool_outputs >= 50000:
                        tf = tempfile.NamedTemporaryFile()
                        with io.open('/home/astrid' + tf.name+'.json', 'w',encoding='utf8' ) as f:                            
                            f.write(json.dumps(res, ensure_ascii=False))
                            pool_outputs = []
    
            p.close()
            p.join()
                  
            for p in active_children():
               p.terminate()
                           
        except KeyboardInterrupt:
            sys.exit(1)
                  
        except Exception as e:
            print e
            print traceback.print_exc()    


analyse()
