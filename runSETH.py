import sys
import cPickle as pickle
from os import listdir, walk, remove, chdir
import os
from os.path import isfile, join, splitext
import traceback
import progressbar
import pubmed_parser as pp
import re
from subprocess import Popen, PIPE
#===============================================================================
# multiprocessing
#===============================================================================
from multiprocessing import Pool, Queue, Process, Manager, current_process, active_children # use threads
try:
    from urllib import urlretrieve
except ImportError: # Python 3
    from urllib.request import urlretrieve
import tqdm

sys.path.insert(0, '../')
from _config import *

print "Preparing Input Files"
mutations_genview = pickle.load(open(PUBTATOR_INFO['MUTATIONS']['gv'], 'rb'))
inputFiles = []
inputFilesPMC = []

inputFiles.extend([PubMed_DLPath+files for files in listdir(PubMed_DLPath) if isfile(join(PubMed_DLPath, files))])
inputFilesPMC.extend([PMC_DLPath+files for files in listdir(PMC_DLPath) if isfile(join(PMC_DLPath, files))])    
chunks = [inputFilesPMC[x:x+30000] for x in range(0, len(inputFilesPMC), 30000)]


def getArticle(file):

    _, extension = splitext(file)
    
    items=None

    try:
        items = pp.parse_medline_xml(file)
        items = [ 
                        {'pmid':x['pmid'].strip(), 
                        'title': x['title'], 
                        'abstract': x['abstract']} 
                        for x in items if x['pmid'].strip() and x['pmid'].strip() not in mutations_genview and x['delete'] is False]            
    except Exception as e:
        print e
        traceback.print_exc()

    return items

def getArticlePMC(filesList):

    
    items=None

    try:
        
        for file in filesList:        
            content=open(file,'rb')
            items = pp.parse_pubmed_xml(content)
            if isinstance(items, dict):
                items=[items]
            items = [                             
                            {'pmid':x['pmid'].strip(), 
                            'title': x['full_title'], 
                            'abstract': x['abstract']} for x in items if x['pmid'].strip() not in mutations_genview]

    except Exception as e:
        print e
        traceback.print_exc()
        
    return items

def getNER_SETH(text):

    foundMutations = []
    rg = re.compile('(text)=([0-9a-zA-Z_.:>< ]+)')

    try:

        chdir(SETH)        
        cmd = 'java -cp seth.jar seth.ner.wrapper.SETHNERAppMut "{}"'.format(text)
        #proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, preexec_fn=os.setsid,close_fds=True)
        proc = Popen(cmd, shell=True, stdout=PIPE, stderr=PIPE, preexec_fn=os.setsid, close_fds=True)
        (output, error) = proc.communicate()
        rere = rg.findall(output)
        foundMutations = list(set([item[1] for item in rere]))
        #print text, foundMutations
        chdir(BACKEND)

    except Exception, e:
        print e
        traceback.print_exc()

    return foundMutations


def processFiles(fileName):   
    headers = {
        'Content-Type': 'application/json',
    }
    
    textFields = ['title','abstract']

    bar = progressbar.ProgressBar()
    articles = getArticle(fileName)
    tmpList_json = {}
  
    for article in articles:
        text = u'  '.join([article[x].strip() for x in article if x in textFields]).encode('utf-8').strip()
        tmpList_json[article['pmid']]=getNER_SETH(text)

    return tmpList_json
                
def runSETH(type=1):
    
    
    bar = progressbar.ProgressBar()
    
    #===============================================================================
    # SINGLE
    #===============================================================================
    if type==1:
        for item in bar(inputFiles):
            res = processFiles(item)
            if res:
                mutations_genview.update(res)
 
    #===============================================================================
    # PARALLEL
    #===============================================================================
    
    if type==0:    
    
        p = Pool(processes=22)
        pool_outputs =[]

        headers = {
            'Content-Type': 'application/json',
        }

        try: 
            for res in tqdm.tqdm(p.imap_unordered(processFiles, inputFiles), total=len(inputFiles)):
                if res:
                    mutations_genview.update(res)

            p.close()
            p.join()
            


            for p in active_children():
               p.terminate()

        except KeyboardInterrupt:
            sys.exit(1)

        except Exception as e:
            print e
            print traceback.print_exc()
            
    pickle.dump(gv_mutations, open(PUBTATOR_INFO['MUTATIONS']['gv'],'wb'), protocol=pickle.HIGHEST_PROTOCOL)

runSETH()