import urllib2, ssl, sys, ast
from urllib import urlencode
import requests, time
import cPickle as pickle
sys.path.insert(0, '../')
from _config import *
import os.path
import traceback
import progressbar
import faulthandler
import trace
import csv
import re
import subprocess
from signal import SIGINT, SIGTERM
from os import listdir, remove, kill
from os.path import isfile, join
import json
from random import shuffle
import signal
import envoy
import glob
from tempfile import NamedTemporaryFile, mkdtemp
from unidecode import unidecode
import gc
faulthandler.enable()

#===============================================================================
# multiprocessing
#===============================================================================
from multiprocessing import Pool, Queue, Process, Manager, current_process, active_children, cpu_count , log_to_stderr, get_logger# use threads
try:
    from urllib import urlretrieve
except ImportError: # Python 3
    from urllib.request import urlretrieve    
import tqdm

class getAscoDetails:
    
    def __init__(self):
        #self.text = text
        #self.context = ssl._create_unverified_context()
        self.data =  [ast.literal_eval(item) for item in open(ASCO_DL_FILE,'rb').readlines()]
    #===========================================================================
    # TO DO: CALL SPIDER
    #===========================================================================
    
    
    
    #===========================================================================
    # ANALYZE
    #===========================================================================
    def getNER_Requests(self, nerType, text):
        #=======================================================================
        # nerType =  GNormPlus or tmVar
        #ONLINE -> VERY SLOW
        #=======================================================================
        context = ssl._create_unverified_context()
        headers = { 'User-Agent' : 'Mozilla/5.0' }
        url_Submit = "https://www.ncbi.nlm.nih.gov/CBBresearch/Lu/Demo/RESTful/tmTool.cgi/{}/Submit/".format(nerType)
        urllib_submit = urllib2.urlopen(url_Submit, data=text, context=context)
    
        SessionNumber = urllib_submit.read()
        url_Receive = "https://www.ncbi.nlm.nih.gov/CBBresearch/Lu/Demo/RESTful/tmTool.cgi/" + SessionNumber + "/Receive/"
        
        code=404
        while(code == 404 or code == 501):
            #print code
            time.sleep(5)
            try:
                r = requests.get(url_Receive)
                code = r.status_code
            except Exception, e:
                print e
                traceback.print_exc()
                
        text = [item.split('\t') for item in r.text.split('\n')[2:]]
        entities = []
        #entities = [item[3] for item in text if item[4].strip() != u'Species' and item[5].strip() != u'9606']
        for item in text:
            try:
                if item[4].strip() != u'Species' and item[5].strip() != u'9606':
                    entities.append(item[3])
            except IndexError:
                pass
    
        return ",".join(list(set(entities)))
    
    
    def prepareGNP(self, data):
        #===========================================================================
        # GNormPlus NER: DEPRECATED
        #===========================================================================
        print 'Starting GNormPlus on input data'
        keys = {}
        for x in data:
            #x = x.replace('\r', '').replace('\t', '').replace('\n', '').strip()
            #print type(x)
            #try:
            tmp = ast.literal_eval(x)
            #except Exception,e:
            #    print e
            #    print x
            #    raw_input('prompt')
            pmid = tmp['pmid']
            if pmid not in keys:
                with open(inputFolder +'/' + pmid+'.txt','wb') as f:
                    title = "".join(tmp['title'].strip())
                    abstract = tmp['abstract']
                    f.write('{}|t|{}\n'.format(pmid, title))
                    f.write('{}|a|{}\n\n'.format(pmid, abstract))
                    keys[pmid]=True
    
        #===========================================================================
        # CHANGE TO /HOME/GNORMPLUS AND THEN EXECUTE cmd! 
        #===========================================================================
        #cmd = 'java -Xmx10G -Xms10G -jar {} {} {} {} {}'.format(GNP_JAR, GNormPLusIn, GNormPLusOut,GNormPlusSetup,GNormPLusTmp)
        #cmd = 'perl  /home/GNormPlus/GNormPlus.pl -i /home/docClass/resources/tmp/ -o /home/docClass/files/asco/ -s  /home/GNormPlus/setup.txt'
        #output = subprocess.check_output(cmd, shell=True)
        #print output, type(output)

def prepareNEJI(data):
    #===========================================================================
    # GNormPlus NER
    #===========================================================================
    bar = progressbar.ProgressBar()
    print '\tStarting NEJI on input data'
    keys = {}
    #data = [ast.literal_eval(item) for item in data]
    for tmp  in bar(data):
        pmid = tmp['pmid']
        if pmid not in keys:
            with open(inputFolder +'/' + pmid+'.txt','wb') as f:
                title = "".join(tmp['title'].strip())
                abstract = tmp['abstract']
                f.write('{}\n{}'.format(title, abstract))
                #f.write('{}'.format(pmid, abstract))
                keys[pmid]=True
                
    #===========================================================================
    # CHANGE TO /HOME/neji AND THEN EXECUTE cmd! 
    #===========================================================================
    os.chdir(NEJI)
    #cmd = '{} -i {} -if RAW -o {} -of JSON  -m {} -t 20'.format(NEJI_SH, inputFolder, outputFolder, NEJI_MODELS)
    cmd = './neji.sh -i {} -if RAW -o {} -of JSON -d{}  -m {} -t 20'.format( inputFolder, outputFolder, NEJI_DICTIONARIES, 'resources/models/')
    output = subprocess.check_output(cmd, shell=True)
    os.chdir(BACKEND)
    print "NEJI DONE!"
    
def getNER_NEJI(text):
    #===========================================================================
    # CHANGE TO /HOME/neji AND THEN EXECUTE cmd! 
    #===========================================================================
    d=mkdtemp()
    f=NamedTemporaryFile(delete=False, prefix=d+'/')
    f.write(text)
    f.close()

    os.chdir(NEJI)
    #cmd = '{} -i {} -if RAW -o {} -of JSON  -m {} -t 20'.format(NEJI_SH, inputFolder, outputFolder, NEJI_MODELS)
    cmd = './neji.sh -i {} -if RAW -o {} -of JSON -d{}  -m {} -t 20'.format( d, d, NEJI_DICTIONARIES, 'resources/models/')
    output = subprocess.check_output(cmd, shell=True)
    tmp = json.load(open(f.name +'.json', 'rb'))    
    #===========================================================================
    # for item in tmp:
    #     if item['terms']:
    #===========================================================================
    blah = [ x['text'] for x in item['terms'] for item in tmp if item['terms']]    
    os.removedirs(d)
    os.chdir(BACKEND)
    print "NEJI DONE!"
    return blah

def getNER_SETH(text):

    foundMutations = []
    rg = re.compile('(text)=([0-9a-zA-Z_.:>< ]+)')

    try:
        os.chdir(SETH)        
        cmd = 'java -cp seth.jar seth.ner.wrapper.SETHNERAppMut "{}"'.format(text)
        #proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, preexec_fn=os.setsid,close_fds=True)
        proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, preexec_fn=os.setsid, close_fds=True)
        (output, error) = proc.communicate()
        #=======================================================================
        # print 'output\t',output
        # print 'error\t',error
        #=======================================================================
        rere = rg.findall(output)
        foundMutations = list(set([item[1] for item in rere]))
        #proc.terminate()
        os.chdir(BACKEND)

    except Exception, e:
        print e
        traceback.print_exc()

    return foundMutations
    
def annotateGenes_neji(pmid):
#===========================================================================
    # analyze all files in  outputFolder 
    #===========================================================================
    tmp = json.load(open(outputFolder+ pmid +'.json', 'rb'))

    #genes = "".join([ x['text'] for x in item['terms'] for item in z if item['terms']] )
    blah = []
    for item in tmp:
        if item['terms']:
            blah.extend(["".join(x['text']) for x in item['terms']])
    
    remove(outputFolder+ pmid +'.json')
    return ",".join(blah)

def analyzeASCO(tmp):
    
    annotatedArticle = None

    try:
        #print tmp
        tmp ['abstract']= tmp ['abstract'].replace('"', '').replace("`",'') #remove in next iteration, one the first analysis is done
        tmp['mutation_normalizedValue']=getNER_SETH(tmp ['abstract'])
        tmp['gene_name']=annotateGenes_neji(tmp['pmid'])
        tmp['source']= 'ASCO'
        annotatedArticle = tmp

    except SyntaxError, e:
        print e
        traceback.print_exc()

    return annotatedArticle

#===============================================================================
#    
#    UNCOMMET TO PREPARE ASCO IF WE DECIDE TO INCLUDE ASCO IN THE GAME AGAIN
#
# if __name__ == '__main__':
#     
#     
#     #===============================================================================
#     # RUN NER ON DOWNLOADED ASCO CORPUS
#     #===============================================================================
#     #data = shuffle([ast.literal_eval(item) for item in open(ASCO_DL_FILE,'rb').readlines()])
#     data = [ast.literal_eval(item) for item in open(ASCO_DL_FILE,'rb').readlines()]
#     prepareNEJI(data)
#     
#     #===============================================================================
#     # ANNOTATE DOWNLOADED ASCO CORPUS WITH MUTATION AN GENE NAMES
#     #===============================================================================
#     #annotatedCorpus = analyzeASCO(data)
#     
#     #===============================================================================
#     # PARALLEL
#     #===============================================================================
#     
#     try: 
#         
#         print "\tAnnotationg ASCO articles with gene and mutation mentions."
#         #log_to_stderr()
#         #logger = get_logger()
#         #logger.setLevel(logging.DEBUG)        
# 
#         jobs = []
#         p = Pool(10)
#         pool_outputs =[]
#         pool_outputs_none=0
#         #log_to_stderr(logging.DEBUG)
#         
#         for res in tqdm.tqdm(p.imap(analyzeASCO, data), total=len(data)):
#             #print res['abstract']
#             if res is not None:
#                 pool_outputs.append(res)
#             else:
#                 pool_outputs_none += 1
#  
#         p.close()
#         p.join()
#         
#         for p in active_children():
#            p.terminate()        
#          
#         pickle.dump(pool_outputs, open(ASCO_PREPARED_FILE, 'wb'))
#         #remove(ASCO_DL_FILE)
#         
#         files = glob.glob(inputFolder)
#         for f in files:
#             remove(f)
#             
#         files = glob.glob(outputFolder)
#         for f in files:
#             remove(f)
#             
#         print "Analyzed:\t {}".format(len(pool_outputs))
#         print "Not analyzed:\t {}".format(pool_outputs_none)
#         print "\tASCO ready"
#              
#     except KeyboardInterrupt:
#         sys.exit(1)
#              
#     except Exception as e:
#         print e
#         print traceback.print_exc()
#===============================================================================