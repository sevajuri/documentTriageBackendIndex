#from _config import *
import sys
from ftplib import FTP, error_perm
import wget, csv, shutil
import cPickle as pickle
from os import listdir, remove
from os.path import isfile, join, exists, splitext
import tarfile
import time
import funcy 
import ftplib
import tqdm

sys.path.insert(0, '../')
from _config import *


#===============================================================================
# multiprocessing
#===============================================================================
from multiprocessing import Pool, Queue, Process, Manager, current_process, active_children # use threads
try:
    from urllib import urlretrieve
except ImportError: # Python 3
    from urllib.request import urlretrieve

#===============================================================================
# config variables
#===============================================================================
def start_process():
    print 'Starting', current_process().name
    
result_list = []
def log_result(result):
    # This is called whenever foo_pool(i) returns a result.
    # result_list is modified only by the main process, not the pool workers.
    result_list.append(result)    

def downloadPartial(parameters):
    
    ret = {}
    
    try:
        
        ftp = ftplib.FTP('ftp.ncbi.nlm.nih.gov', timeout=15)
        ftp.login()
        
        path, filename = parameters
        links = path.rsplit('/',1)
        #print 'Started with {}'.format(filename)
        start_time = time.time()
        downLocation = RESOURCES+'tmp/'+links[1]
        
        #urlretrieve(filename[0], filename=downLocation)
        ftp.cwd(links[0])
        ftp.retrbinary("RETR " + links[1] ,open(downLocation, 'wb').write)
        
        #print 'Downloaded {}'.format(filename)
        tar = tarfile.open(downLocation, "r:gz")

        members=[]
        for x in tar.getmembers():
            if x.name[-5:]=='.nxml':
                x.name = filename+'.nxml'
                members.append(x)
                ret[filename]=True
                
        #print 'Got all files{}'.format(filename)
        tar.extractall(path=PMC_DLPath,members=members)
        remove(downLocation)
        #print 'Done with {} in {}'.format(filename, time.time()-start_time)
        #queue.put(ret)
        #return ret
        ftp.close()
        return ret

    except Exception as e:
        print e
        return None
    
    
def downloadBulk(url):
    
    tempDict = {}
    #print 'Downloading {}'.format(url)
    downLocation = TMP+url.split('/')[-1]
    filename  = wget.download(url,out=downLocation, bar=None)
    tar = tarfile.open(downLocation, "r:gz")
    members = []
    
    #=======================================================================
    # change extraction name 
    #=======================================================================
    for fn in tar.getmembers():
        if fn.isfile():
            fn.name = fn.name.split('/')[-1]
            members.append(fn)
            tempDict[fn.name.split('.')[0]]=True
            
    tar.extractall(path=PMC_DLPath,members=members)
    remove(downLocation)
    return tempDict

#===============================================================================
# downloaded list of files - keep the list in a file instead of actual files TO DO
# AFTER ANALYSIS WITH SETH/NIJI DELETE THE FILES FROM pubmedDLPath
#===============================================================================
alreadyDownDict = {}
if isfile(alreadyDownPMC):
    alreadyDownDict = pickle.load(open(alreadyDownPMC, 'rb'))

#===========================================================================
# INITIAL DOWNLOAD
#===========================================================================

if not alreadyDownDict:
    print "Starting bulk download"
    
    urls = ["ftp://ftp.ncbi.nlm.nih.gov/pub/pmc/articles.A-B.xml.tar.gz",
            "ftp://ftp.ncbi.nlm.nih.gov/pub/pmc/articles.C-H.xml.tar.gz",
            "ftp://ftp.ncbi.nlm.nih.gov/pub/pmc/articles.I-N.xml.tar.gz",
            "ftp://ftp.ncbi.nlm.nih.gov/pub/pmc/articles.O-Z.xml.tar.gz"]
    p = Pool(processes=20)
    pool_outputs =[]
    try: 

        for res in tqdm.tqdm(p.imap_unordered(downloadBulk, urls), total=len(urls)):
            if res: 
                pool_outputs.append(res)
                
        p.close()
        p.join()
        
        if pool_outputs:
            try:
                
                alreadyDownDict.update(funcy.join(pool_outputs))
                print '\nDict items after download {}:'.format(len(alreadyDownDict)) #
            except ValueError as e:
                print e
                traceback.print_exc()
                print pool_outputs
        else:
            print 'Nothing to write to dict'
                
    except KeyboardInterrupt:
        sys.exit()

else:
    #===========================================================================
    # if previosly downloaded the bulk download get only the files which are new and get them from their ftp locations
    #===========================================================================
    print 'Downloaded: {}'.format(len(alreadyDownDict)) #1488928

    file = wget.download('ftp://ftp.ncbi.nlm.nih.gov/pub/pmc/oa_comm_use_file_list.csv',out=fileList_PMC)
    if exists(fileList_PMC):
        shutil.move(file,fileList_PMC)

    with open(fileList_PMC, 'rb') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',')
        header = spamreader.next()
        listedDoc = [('pub/pmc/'+row[0],row[2]) for row in spamreader if row[2] not in alreadyDownDict]

    print '\nTo download {}:'.format(len(listedDoc))
    p = Pool(processes=20)
    pool_outputs =[]
    try: 

        for res in tqdm.tqdm(p.imap_unordered(downloadPartial, listedDoc), total=len(listedDoc)):
            if res: 
                pool_outputs.append(res)
                
        #p.terminate()
        p.close()
        p.join()
        
        for p in active_children():
           p.terminate()
        
        if pool_outputs:
            try:
                alreadyDownDict.update(funcy.join(pool_outputs))
            except ValueError as e:
                print e
                traceback.print_exc()
                print pool_outputs
                
            
                
    except KeyboardInterrupt:
        sys.exit()

pickle.dump(alreadyDownDict,open(alreadyDownPMC, 'wb'))
print '\nDict items after download {}:'.format(len(alreadyDownDict))