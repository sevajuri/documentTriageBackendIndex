import sys, wget, shutil, gzip, traceback
import cPickle as pickle
from os import remove
from os.path import exists
import MySQLdb as mdb

sys.path.insert(0, '../')
from _config import *

def getAdditionalInfo(url, downloadFile, pickleFile):
    
    dict = {}
    file = wget.download(url,out=downloadFile)
    if exists(downloadFile):
        shutil.move(file,downloadFile)
        
    with gzip.open(downloadFile, 'rb') as f:
        
        for row in f.readlines():    
            pmid,_,mutations,_=row.split('\t')
            pmid = pmid.strip()
            
            if pmid not in dict:
                dict[pmid]=mutations
            else:
                dict[pmid]=dict[pmid]+','+mutations
    pickle.dump(dict,open(pickleFile, 'wb'))
    remove(downloadFile)
    
def journalsList2Pickle():
    journalsDict = {}
    invertedJournal ={}
    f = open(JOURNAL_LIST,'rb')
    fc = f.read().split('--------------------------------------------------------')
    journalList = []
    for item in fc:
        if item:
            dict = {} 
            for x in  item.splitlines():
                if x:
                    dummy = x.split(":", 1)
                    dict[dummy[0]]=dummy[1]

            journalsDict[dict['JournalTitle'].strip()]={ k.strip():v.strip() for k,v in dict.iteritems() if k != 'JournalTitle' }

    invertedJournals = {}
    for k,v in journalsDict.iteritems():
        for a,b in v.iteritems():
            if b.strip() and b.strip() not in invertedJournals:
                invertedJournals[b.strip()]=k.strip()
            
    pickle.dump(journalsDict,open(JOURNAL_PICKLE, 'wb'))
    pickle.dump(invertedJournals,open(JOURNAL_PICKLE_INVERTED, 'wb'))
    remove(JOURNAL_LIST)

def journalsList2DB():
    f = open(JOURNAL_LIST,'rb')
    fc = f.read().split('--------------------------------------------------------')
    journalList = []
    for item in fc:
        if item:
            dict = {} 
            for x in  item.splitlines():
                if x:
                    dummy = x.split(":")
                    dict[dummy[0]]=dummy[1]
            try:
                journalList.append((dict['JrId'].strip(),
                              dict['MedAbbr'].strip(),
                              dict['JournalTitle'].strip(),
                              dict['ISSN (Print)'].strip(),
                              dict['ISSN (Online)'].strip(),
                              dict['NlmId'].strip()
                            ))
            except ValueError:
                traceback.print_exc()
    
    con = mdb.connect('localhost', 'root', 'root', 'cancer', use_unicode=True, charset='utf8');
    cur = con.cursor(mdb.cursors.DictCursor)
     
    cur.executemany("""REPLACE INTO journals 
                    (journals_id,journals_short,journals_full,journals_issn_print,journals_issn_online,journals_NlmId) 
                    VALUES (%s,%s,%s,%s,%s,%s)""", journalList)
    con.commit()
    con.close()        
    
for k,v in PUBTATOR_INFO.iteritems():
    print "Downloading {}\n".format(v['downloadFile'])
    getAdditionalInfo(v['url'], v['downloadFile'], v['pickleFile'])
    print "Finished downloading {}\n".format(v['downloadFile'])

file = wget.download(JOURNAL_LIST_URL,out=JOURNAL_LIST)
print "Downloading {}\n".format(JOURNAL_LIST)
if exists(JOURNAL_LIST):
    shutil.move(file,JOURNAL_LIST)
print "Finished downloading {}\n".format(JOURNAL_LIST)
    
journalsList2Pickle()        