import os
import pkg_resources
import _pickle as pickle
import traceback
import sys
from os import listdir, walk, remove, chdir
from os.path import isfile, join, splitext
import traceback
import progressbar
import pubmed_parser as pp
import re
import gc

#from nalaf.utils.readers import TextFilesReader, PMIDReader
from nalaf.utils.readers import StringReader
from nalaf.utils.writers import ConsoleWriter#, TagTogFormat, PubTatorFormat
from nalaf.learning.crfsuite import PyCRFSuite
from nala.utils import PRO_CLASS_ID, MUT_CLASS_ID, PRO_REL_MUT_CLASS_ID, get_prepare_pipeline_for_best_model
#from nalaf.domain.bio.gnormplus import GNormPlusGeneTagger
from nalaf.learning.taggers import StubSameSentenceRelationExtractor
from nala.learning.postprocessing import PostProcessing

#===============================================================================
# multiprocessing
#===============================================================================
from multiprocessing import Pool, Queue, Process, Manager, current_process, active_children # use threads
try:
    from urllib import urlretrieve
except ImportError: # Python 3
    from urllib.request import urlretrieve
import tqdm

sys.path.insert(0, '../')
from _config import *

import warnings
warnings.filterwarnings("ignore")

try:
    mutations_nala = pickle.load(open(PUBTATOR_INFO['MUTATIONS']['nala'], 'rb'))
except IOError:
    mutations_nala = pickle.load(open(PUBTATOR_INFO['MUTATIONS']['gv'], 'rb'), encoding='utf-8')
    

print('LOADING STUFF I NEED')
inputFiles = []
inputFiles.extend([  (PubMed_DLPath+files, 'PUBMED') for files in listdir(PubMed_DLPath) if isfile(join(PubMed_DLPath, files))   ])
inputFilesPMC = [PMC_DLPath+files for files in listdir(PMC_DLPath) if isfile(join(PMC_DLPath, files))]    
chunks = [ (inputFilesPMC[x:x+30000], 'PMC') for x in range(0, len(inputFilesPMC), 30000)]
inputFiles.extend(chunks)
del inputFilesPMC
del chunks
gc.collect()

pipeline = get_prepare_pipeline_for_best_model()
crf = PyCRFSuite()
bin_model = pkg_resources.resource_filename('nala.data', 'default_model')

print('STUFF LOADED')
    
def getArticle(file):

    #print('Getting articles from file(s)')
    items=None

    if file[1]== 'PUBMED':
        try:
            items = pp.parse_medline_xml(file[0])
            items = [ 
                            {'pmid':x['pmid'].strip(), 
                            'title': x['title'], 
                            'abstract': x['abstract']} 
                            for x in items if x['pmid'].strip() and x['pmid'].strip() not in mutations_nala and x['delete'] is False and x['abstract'].strip()]             
        except Exception as e:
            print(e)    
            traceback.print_exc()
            
    if file[1]== 'PMC':
        try:            
            items=[]
            for fil in file[0]:        
                item = pp.parse_pubmed_xml(fil)
                if item['pmid'].strip() not in mutations_nala and item['abstract'].strip():
                    items.append(
                                            {'pmid':item['pmid'].strip(), 
                                            'title': item['full_title'], 
                                            'abstract': item['abstract']}
                        )

        except Exception as e:
            print(e)    
            traceback.print_exc()        

    return items


def write2Disk(file):
    
    articles = getArticle(file)
    for article in articles:
        pass
#===============================================================================
#         f = open(nalaIn+article['pmid'].strip()+'.txt', 'w')
#         text = " ".join( [article['title'], article['abstract'] ] ).strip()
# 
#         f.write(text)
#         f.close()
#===============================================================================
        
def nalaDisk(file):
    
    articles = getArticle(file)
    for article in articles:
        
        f = open(nalaIn+article['pmid'].strip()+'.txt', 'w')
        text = " ".join( [article['title'], article['abstract'] ] ).strip()

        f.write(text)
        f.close()        

def runNala(items):

    #print('Runing NALA')
    #===========================================================================
    # pipeline = get_prepare_pipeline_for_best_model()
    # crf = PyCRFSuite()
    # bin_model = pkg_resources.resource_filename('nala.data', 'default_model')
    #===========================================================================
    textFields = ['title','abstract']
    annotationDict = {}
    for article in items:
        text = " ".join( [article['title'], article['abstract'] ] ).strip()
        #print('NALA start\t',article, '\n', text)
        dataset = StringReader(text).read()
        pipeline.execute(dataset)
        crf.tag(dataset, bin_model, MUT_CLASS_ID)
        PostProcessing().process(dataset)
        x=ConsoleWriter(ent1_class_id=PRO_CLASS_ID, ent2_class_id=MUT_CLASS_ID, color=False).getAnotations(dataset)
        #print('annos\t',x)
        annotationDict[article['pmid']]=x
        #print('annos\t',x)
        #print(annotationDict[article['pmid']])
        #input('nala')
    return annotationDict

def processFiles(fileName):   
    headers = {
        'Content-Type': 'application/json',
    }
    
    tmpList_json = {}
    textFields = ['title','abstract']
    articles = getArticle(fileName)
    if articles is not None:
        tmpList_json=runNala(articles)        
    return tmpList_json


def runAnnotate(type=1):
    
    bar = progressbar.ProgressBar()
    
    #===============================================================================
    # SINGLE
    #===============================================================================
    if type==1:
        for item in bar(inputFiles):
            res = processFiles(item)
            if res:
                mutations_nala.update(res)
 
    #===============================================================================
    # PARALLEL
    #===============================================================================
    
    if type==0:
 
        p = Pool(processes=10)
        pool_outputs =[]
     
        headers = {
            'Content-Type': 'application/json',
        }
     
        try: 
            for res in tqdm.tqdm(p.imap_unordered(write2Disk, inputFiles), total=len(inputFiles)):
                if res:
                    mutations_nala.update(res)
     
            p.close()
            p.join()
             
            pickle.dump(mutations_nala, PUBTATOR_INFO['MUTATIONS']['nala'], 2)
             
            for p in active_children():
               p.terminate()
     
        except KeyboardInterrupt:
            sys.exit(1)
     
        except Exception as e:
            print(e)    
            print(traceback.print_exc())    

runAnnotate(0)