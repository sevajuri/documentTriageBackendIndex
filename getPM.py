import ftplib
import time
import cPickle as pickle
import traceback
from os import listdir
from os.path import isfile, join
import progressbar
import sys
import funcy


#from _config import *
sys.path.insert(0, '../')
from _config import *

#===============================================================================
# multiprocessing
#===============================================================================
from multiprocessing import Pool, Queue, Process, Manager, current_process, active_children # use threads
try:
    from urllib import urlretrieve
except ImportError: # Python 3
    from urllib.request import urlretrieve
    
import tqdm
    

#===============================================================================
# PARAMS
#===============================================================================
alreadyDownDict = {}
if isfile(alreadyDownPubMed):
    #f = open(pubmedListFile)
    alreadyDownDict = pickle.load(open(alreadyDownPubMed, 'rb'))

print 'Downloaded: {}'.format(len(alreadyDownDict.keys()))

#===============================================================================
# LIST OF FILES 2 DOWNLOAD
#===============================================================================
paths = ['pubmed/baseline/', 'pubmed/updatefiles/']
downloadList = []

#onDiskList = [files for files in listdir(PubMed_DLPath) if isfile(join(PubMed_DLPath, files))]
#for item in onDiskList:
#    alreadyDownDict[item]=True

def getDownFileList(path, alreadyDownDict):

    files = []
    ftp=ftplib.FTP('ftp.ncbi.nlm.nih.gov', timeout=15)
    ftp.login()
    ftp.cwd(path)
    files = [(path, item) for item in ftp.nlst() if item[-2:] =='gz' and item not in alreadyDownDict]
    ftp.close()
    return files

for x in paths:
    downloadList.extend(getDownFileList(x, alreadyDownDict))

#===============================================================================
# download files  which are not downloaded
#===============================================================================
def downloadFiles(params):
    path, filename = params
    ret = {}

    try:
        
        ftp=ftplib.FTP('ftp.ncbi.nlm.nih.gov', timeout=15)
        ftp.login()
        ftp.cwd(path)        
        
        with open(PubMed_DLPath + filename, 'wb') as f:
            #print 'Downloading {}'.format(filename)
            ftp.retrbinary('RETR ' + filename, f.write)
            ret[filename]=True
        
        ftp.close()       
        return ret
    
    except Exception as e:
        print e
        return None
 
#===============================================================================
# PARALLEL
#===============================================================================
p = Pool(processes=20)

pool_outputs =[]
try: 
    if downloadList:
        for res in tqdm.tqdm(p.imap_unordered(downloadFiles, downloadList), total=len(downloadList)):
            if res is not None: 
                pool_outputs.append(res)
                 
        #p.terminate()
        p.close()
        p.join()
        
        for p in active_children():
           p.terminate()        
         
        #print pool_outputs
        if pool_outputs:
            try:
                
                alreadyDownDict.update(funcy.join(pool_outputs))
                print '\nDict items after download {}:'.format(len(alreadyDownDict)) #
            except ValueError as e:
                print e
                traceback.print_exc()
                print pool_outputs
        else:
            print 'Nothing to write to dict'
    else:
        print 'No new files for download'
             
except KeyboardInterrupt:
    sys.exit()
    
pickle.dump(alreadyDownDict, open(alreadyDownPubMed,'wb'))
print 'Dict pickled'
