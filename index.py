from os import listdir, walk, remove
from os.path import isfile, join, splitext
import requests
#import xml.etree.ElementTree as ET
from sklearn.externals import joblib
from sklearn.feature_extraction.text import CountVectorizer
import pubmed_parser as pp
import MySQLdb as mdb
import sys, psutil, gzip, csv, json
from asyncore import write
import traceback, time
import cPickle as pickle
from unidecode import unidecode
#from ascoParse import getAscoDetails
from ascoParse import getNER_SETH, getNER_NEJI
import progressbar
import faulthandler
import trace
faulthandler.enable()
from ascoParse import *
import gc
#===============================================================================
# multiprocessing
#===============================================================================
from multiprocessing import Pool, Queue, Process, Manager, current_process, active_children # use threads
try:
    from urllib import urlretrieve
except ImportError: # Python 3
    from urllib.request import urlretrieve
import tqdm
sys.path.insert(0, '../')
from _config import *

#===============================================================================
# CLASSIFICATION STUFF
#===============================================================================
print "LOADING CLASSIFICATION MODELS AND VARIOUS DICTIONARIES"
isCancer_model = joblib.load(open(BEST_IS,'rb'))
isCancer_vectorizer = joblib.load(open(NGRAM_IS,'rb'))

whichCancer_model = joblib.load(open(BEST_WHICH,'rb'))
whichCancer_vectorizer = joblib.load(open(NGRAM_WHICH,'rb'))

genes_pubtator = pickle.load(open(PUBTATOR_INFO['GENES']['pickleFile'], 'rb'))
mutations_pubtator = pickle.load(open(PUBTATOR_INFO['MUTATIONS']['pickleFile'], 'rb'))
genes_genview = pickle.load(open(PUBTATOR_INFO['GENES']['gv'], 'rb'))
mutations_genview = pickle.load(open(PUBTATOR_INFO['MUTATIONS']['gv'], 'rb'))
journalList = pickle.load(open(JOURNAL_PICKLE, 'rb'))
journalListInverted = pickle.load(open(JOURNAL_PICKLE_INVERTED, 'rb'))

#===============================================================================
# database stuff
#===============================================================================.
def getJournalID(title):
    
    con = mdb.connect('localhost', 'root', 'root', 'cancer', use_unicode=True, charset='utf8');
    cur = con.cursor(mdb.cursors.DictCursor)
    journals_NlmId = ''
    try:
        cur.execute("SELECT journals_NlmId FROM journals where journals_full = '{}'".format(title.strip()))
        if cur.rowcount != 0:
            journals_NlmId = cur.fetchone()['journals_NlmId']
        con.close()
    except Exception, e:
        #print 'getGenesMutations',pmid,e, traceback.print_exc()
        #raw_input('prompt')
        logger_deubg.debug('{}\n{}\n{}\n{}'.format('getGenesMutations',pmid,e, traceback.print_exc()))
        
    return journals_NlmId

def normalizeJournalTitle(article):
    #===========================================================================
    # MAPPINGS
    #===========================================================================
    journal = ''    
    fieldMappings = [
            'medline_ta',
            'issn_linking',
            'nlm_unique_id',            
            'issn_epub', 
            'issn_ppub'
            ]

    for item in fieldMappings:
        try:
            journal = journalListInverted[article[item].strip()].strip()
            #===================================================================
            # if journal != '':
            #     print 'found:\t', item
            #===================================================================
            break
        except KeyError as e:
            pass

    if journal =='':
        journal = article['journal']
        
    return journal

def getMutations(text, pmid):
    
    mutations = []
    mutationsGV=None
    mutationsPubtator=None
    
    #===========================================================================
    # GV / SETH
    #===========================================================================
    
    try:
        mutationsGV=mutations_genview[pmid]
    except KeyError:
        #mutationsGV=getNER_SETH(text)
        pass
        
    if mutationsGV:
        mutations.extend(mutationsGV)

    #===========================================================================
    # PUBTATOR/TMVAR
    #===========================================================================
    try:
        pubtatorAnnotations = [item.split('|') for item in mutations_pubtator[pmid].split(',')]
        mutationsPubtator = [val.strip() for sublist in pubtatorAnnotations for val in sublist if val]
    except (KeyError, UnicodeDecodeError):
        pass
    
    if mutationsPubtator:
            mutations.extend(mutationsPubtator)    
    
    return list(set(mutations))
    

def getGenes(text, pmid):


    genes = []
    genesGV=None
    genesPubtator=None
    
    #===========================================================================
    # GV / NEJI
    #===========================================================================    
    try:
        genesGV=genes_genview[pmid]
    except KeyError:
        #genesGV=getNER_NEJI(text)
        pass
        
    if genesGV:
        genes.extend(genesGV)

    #===========================================================================
    # PUBTATOR/TMVAR
    #===========================================================================
    try:
        pubtatorAnnotations = [item.split('|') for item in genes_pubtator[pmid].split(',')]
        genesPubtator = [val.strip() for sublist in pubtatorAnnotations for val in sublist if val]
    except (KeyError, UnicodeDecodeError):
        pass
    
    if genesPubtator:
            genes.extend(genesPubtator)    
    
    return list(set(genes))
    
def getAnotations(pmid, pickleFile, informationSource):
    #===========================================================================
    #     PMID -> article PMID
    #    pickleFile -> pickled pubtator files, two values 'genes or mutations
    #    informationSource -> 'genes' or 'mutations'
    #===========================================================================
    dbSources ={        
        'genes':'hugo',
        'mutations':'normalized'
    }
    
    annotations =[]
    if pmid != u'-1':
        try:
            con = mdb.connect('localhost', 'root', 'root', 'cancer', use_unicode=True, charset='utf8');
            cur = con.cursor(mdb.cursors.DictCursor)
            query = ""
            cur.execute("SELECT {} from {} where pmid={}".format(dbSources[informationSource], informationSource, pmid))
            if cur.rowcount != 0:
                items = cur.fetchall()
                annotations = [x[dbSources[informationSource]].strip() for x in items if x[dbSources[informationSource]] is not None]
            con.close()
            
            try:
                pubtatorAnnotations = [item.split('|') for item in pickleFile[pmid].split(',')]
                flattened = [val.strip() for sublist in pubtatorAnnotations for val in sublist if val]
                annotations.extend(flattened)
            except (KeyError, UnicodeDecodeError):
                pass

        except Exception, e:
            #print 'getAnotations',pmid,e, traceback.print_exc()
            #raw_input('prompt')
            logger_deubg.debug('{}\n{}\n{}\n{}'.format('getAnotations',pmid,e, traceback.print_exc()))
    
    return list(set(annotations))

def classifyArticle(article):
    
    tmp = None
    textFields = ['title', 'full_title','abstract']

    try:
         text = ' '.join([article[x].strip() for x in article if x in textFields]).strip()
         transformed_TrainText = isCancer_vectorizer.transform([text])
         result = isCancer_model.predict(transformed_TrainText)
         result_proba = isCancer_model.predict_proba(transformed_TrainText)
        #result_proba_log = isCancer_model.predict_log_proba(transformed_TrainText)
         classIndex = isCancer_model.classes_.tolist().index(result[0])
         #print isCancer_model.classes_.tolist(), result[0], classIndex, result_proba.tolist()[0][classIndex], result_proba_log.tolist()[0][classIndex]       

         if result[0] == 'notCancer':
             article['confidence_is']= result_proba.tolist()[0][classIndex]*-1
             article['cancerType']=u'Not cancer'
             article['confidence_which']= int(0)

         if result[0] == 'cancer':
            article['confidence_is']= result_proba.tolist()[0][classIndex]
            transformed_WhichCancer = whichCancer_vectorizer.transform([text])
            result_which = whichCancer_model.predict(transformed_WhichCancer)
            result_which_proba = whichCancer_model.predict_proba(transformed_WhichCancer)
            #result_which_proba_log = whichCancer_model.predict_log_proba(transformed_WhichCancer)
            whichIndex = whichCancer_model.classes_.tolist().index(result_which[0])
            article['cancerType']= result_which[0].decode('utf-8')
            article['confidence_which']= int(result_proba.tolist()[0][classIndex])
            #print whichCancer_model.classes_.tolist(), result_which[0], whichIndex, result_which_proba.tolist()[0][whichIndex], result_which_proba_log.tolist()[0][whichIndex]

         tmp= article

    except Exception as e:
        #print 'classifyArticle',article,e, traceback.print_exc()
        #raw_input('prompt')
        logger_deubg.debug('{}\n{}\n{}\n{}'.format('classifyArticle',article,e, traceback.print_exc()))

    return tmp

def normalizeKeys(article):

    try:
        if article:
            
            if 'title' not in article and 'full_title' in article:
                article['title']=article['full_title']
    
            if 'mesh_terms' not in article:
                article['mesh_terms']=u''
    
            if 'pubdate' not in article and 'publication_year' in article:
                article['pubdate']=article['publication_year']
    
            if article['journal'].strip() not in journalList and article['source'] != 'ASCO':
                #===================================================================
                # NOT NORMALIZING ASCO JOURNALS
                #===================================================================
                article['journal'] = normalizeJournalTitle(article)
    
            if 'pmid' not in article or ('pmid' in article and not article['pmid']):
                 article['pmid']=u'-1'
    
            if 'author_list' in article:
                try:
                    article['authors'] = ','.join(set([item[1]+' '+item[0] for item in article['author_list']]))
                except TypeError:
                  article['authors'] = u''
    
            if article['mesh_terms']:
                article['mesh_terms'] = u','.join(article['mesh_terms'].split(';'))
    
            try:
                article['pubdate'] = int(article['pubdate'])
            except ValueError:
                try:
                    article['pubdate'] = int(article['pubdate'].split('-')[0])
                except ValueError:
                    article['pubdate']=1
            except TypeError:
                #print article
                article['pubdate']=1
    
            if 'source' not in article:
                article['source']= 'pmc'
                    
            if 'articleLink' not in article:
                article['articleLink']=u'https://www.ncbi.nlm.nih.gov/pubmed/{}'.format(article['pmid'])
           
            textFields = ['title', 'full_title','abstract']
            text = u'  '.join([article[x].strip() for x in article if x in textFields]).encode('utf-8').strip()
    
            if 'gene_name' not in article:
                #article['gene_name']= getAnotations(article['pmid'], genes_pubtator, 'genes')
                article['gene_name']=getGenes(text, article['pmid'].strip())
    
            if 'mutation_normalizedValue' not in article:
                #article['mutation_normalizedValue']=getAnotations(article['pmid'], mutations_pubtator, 'mutations')
                article['mutation_normalizedValue']=getMutations(text, article['pmid'].strip())        
            
            return article

    except Exception as e:
        #print 'normalizeKeys',article,e, traceback.print_exc()
        #raw_input('prompt')
        logger_deubg.debug('{}\n{}\n{}\n{}'.format('normalizeKeys',article,e, traceback.print_exc()))
        
        
def decodeKeys(article):
    try:
         for k in article:
             if isinstance(article[k], basestring):
                article[k]=article[k].strip()
                if type(article[k]) is str:
                    try:
                        article[k]=article[k].decode('utf-8')
                    except AttributeError:
                        pass
    except Exception as e:
        #print 'decodeKeys',article,e, traceback.print_exc()
        #raw_input('prompt')
        logger_deubg.debug('{}\n{}\n{}\n{}'.format('decodeKeys',article,e, traceback.print_exc()))

    return article    


def normalizeKeys_batch(articles):
    
    tmp = []
    
    for i, article in enumerate(articles):
        if article:
            try:
                    #print article.keys()
    
                    if 'title' not in article and 'full_title' in article:
                        article['title']=article['full_title']
            
                    if 'mesh_terms' not in article:
                        article['mesh_terms']=u''
            
                    if 'pubdate' not in article and 'publication_year' in article:
                        article['pubdate']=article['publication_year']
            
                    if article['journal'].strip() not in journalList and article['source'] != 'ASCO':
                        #===================================================================
                        # NOT NORMALIZING ASCO JOURNALS
                        #===================================================================
                        article['journal'] = normalizeJournalTitle(article)
            
                    if 'pmid' not in article or ('pmid' in article and not article['pmid']):
                         article['pmid']=u'-1'
            
                    if 'author_list' in article:
                        try:
                            article['authors'] = ','.join(set([item[1]+' '+item[0] for item in article['author_list']]))
                        except TypeError:
                          article['authors'] = u''
            
                    if article['mesh_terms']:
                        article['mesh_terms'] = u','.join(article['mesh_terms'].split(';'))
            
                    try:
                        article['pubdate'] = int(article['pubdate'])
                    except ValueError:
                        try:
                            article['pubdate'] = int(article['pubdate'].split('-')[0])
                        except ValueError:
                            article['pubdate']=1
                    except TypeError:
                        #print article
                        article['pubdate']=1
            
                    if 'source' not in article:
                        article['source']= 'pmc'
                            
                    if 'articleLink' not in article:
                        article['articleLink']=u'https://www.ncbi.nlm.nih.gov/pubmed/{}'.format(article['pmid'])
                   
                    textFields = ['title', 'full_title','abstract']
                    text = u'  '.join([article[x].strip() for x in article if x in textFields]).encode('utf-8').strip()
            
                    if 'gene_name' not in article:
                        #article['gene_name']= getAnotations(article['pmid'], genes_pubtator, 'genes')
                        article['gene_name']=getGenes(text, article['pmid'].strip())
            
                    if 'mutation_normalizedValue' not in article:
                        article['mutation_normalizedValue']=getMutations(text, article['pmid'].strip())        
                        
                    article = {item: article[item] for item in SOLR_FIELDS if item in article }                    
                    tmp.append(article)
    
            except Exception as e:
                #print 'normalizeKeys',article,e, traceback.print_exc()
                #raw_input('prompt')
                #print article
                logger_deubg.debug('{}\n{}\n{}\n{}'.format('normalizeKeys',article,e, traceback.print_exc()))
                pass

    articles = None
    return tmp


def classifyArticle_batch(articles):
    
    tmp = []
    textFields = ['title', 'full_title','abstract']
    toClassify = []
    #toClassify = [' '.join([article[x].strip() for x in article if x in textFields]).strip() for articles in articles]

    for article in articles:
        #if article:
        tmptext = ' '.join([article[x].strip() for x in article if x in textFields])
        toClassify.append(tmptext)

    #===========================================================================
    # print isCancer_model.classes_
    # print whichCancer_model.classes_
    #===========================================================================

    try:

         transformed_TrainText = isCancer_vectorizer.transform(toClassify)
         result = isCancer_model.predict(transformed_TrainText)
         result_proba = isCancer_model.predict_proba(transformed_TrainText)

         transformed_WhichCancer = whichCancer_vectorizer.transform(toClassify)
         result_which = whichCancer_model.predict(transformed_WhichCancer)
         result_which_proba = whichCancer_model.predict_proba(transformed_WhichCancer)         
         
         #======================================================================
         # print result[:10]
         # print result_proba[:10]
         # 
         # print result_which[:10]
         # print result_which_proba[:10]
         # 
         # raw_input('prompt')
         #======================================================================
         
         for i, ix in enumerate(result):
             
             classIndex = isCancer_model.classes_.tolist().index(ix)
             whichIndex = whichCancer_model.classes_.tolist().index(result_which[i])
                         
             #print i, ix, classIndex, result_proba[i, classIndex]
             #print i, result_which[i], whichIndex, result_which_proba[i, whichIndex]

             if ix == 'notCancer':
                 articles[i]['confidence_is']=  result_proba[i, classIndex]*-1
                 articles[i]['cancerType']=u'Not cancer'
                 articles[i]['confidence_which']= int(0)
                 #print articles[i], "\n-----"

             if ix == 'cancer':
                articles[i]['confidence_is']=  result_proba[i, classIndex]
                articles[i]['cancerType']= result_which[i].decode('utf-8')
                articles[i]['confidence_which']= int(result_which_proba[i, whichIndex])            
                #print articles[i], "\n-----"
                #raw_input('prompt')

    except Exception as e:
        #print 'classifyArticle',article,e, traceback.print_exc()
        #raw_input('prompt')
        logger_deubg.debug('{}\n{}\n{}\n{}'.format('classifyArticle',article,e, traceback.print_exc()))
    
    return articles


        
def getArticle(file):
    #===========================================================================
    # file -> path to downloaded PubMed/PMC files
    #===========================================================================

    _, extension = splitext(file)
    items=None

    try:
        if extension == '.gz':
            content=gzip.open(file)
            items = pp.parse_medline_xml(content)
            items = [dict(x, **{'source':'pubmed'}) for x in items]
            items = [item for item in items if 'delete' not in item or not item['delete']]

        elif extension == '.nxml':
            try:
                #content=open(file,'rb')
                items = pp.parse_pubmed_xml(file)
                #items = [dict(x, **{'source':'pubmed'}) for x in items]
                if isinstance(items, dict):
                    items['source']='pmc'
                    #items=[items]
            except Exception:
                print file

    except Exception as e:
        logger_deubg.debug('{}\n{}\n{}\n{}'.format('getArticle',file,e, traceback.print_exc()))
    
    #print items
    
    return items

def processFiles(fileName):   
    headers = {
        'Content-Type': 'application/json',
    }
    
    gc.collect()
    bar = progressbar.ProgressBar()
    start_time = time.time()
    tmpList_json = []
    
    if isinstance(fileName, list):
        articles = [getArticle(x) for x in fileName]
    else:
        articles = getArticle(fileName)
    
    articles = [x for x in articles if x]
    articles=classifyArticle_batch(articles)
    articles=normalizeKeys_batch(articles)

    try:
        r = requests.post(SOLR_UPDATE, headers=headers, data=json.dumps(articles))
        if r.status_code == 200:
            tmpList_json = []
        else:
            print r.status_code
            print r.text
  
    except Exception as e:
        #print "EXCEPTION", article['pmid'], r.text, e, traceback.print_exc()
        logger_deubg.debug('{}\n{}\n{}\n{}\n{}'.format("EXCEPTION", article['pmid'], r.text, e, traceback.print_exc()))

    else:
        #=======================================================================
        # print 'Outer\t',article
        # raw_input('prompt')
        #=======================================================================
        logger_deubg.debug('EMPTY: {} returns empty'.format(fileName))
                           
    #===========================================================================
    # try:
    #     remove(fileName)
    # except Exception, e:
    #     if isinstance(fileName, list):
    #         remove(ASCO_PREPARED_FILE)
    #===========================================================================

    #logger_deubg.debug('Done with {}\n------------------------------------------------'.format(fileName))
    return tmpList_json


def startAnalysis(type=1):
    
    #===============================================================================
    # INIT
    #===============================================================================
    print "Prepraing Input Files"
    inputFiles = []
    #inputFiles.extend([PubMed_DLPath+files for files in listdir(PubMed_DLPath) if isfile(join(PubMed_DLPath, files))])
    pmcList = [PMC_DLPath+files for files in listdir(PMC_DLPath) if isfile(join(PMC_DLPath, files))]
    chunks = [pmcList[x:x+30000] for x in xrange(0, len(pmcList), 30000)]
    inputFiles.extend(chunks)


    #===========================================================================
    # ascoData = pickle.load(open(ASCO_PREPARED_FILE,'rb'))
    # inputFiles.append(ascoData)
    #===========================================================================

    total_time=time.time()
    counterTime = time.time()
    counter = 0
    revolution = 0
    tmpList = []
    bar = progressbar.ProgressBar()
    print "Indexing {} files".format(len(inputFiles))
    
    #===============================================================================
    # SINGLE
    #===============================================================================
    bar = progressbar.ProgressBar()
    if type==0:
        for item in bar(inputFiles):
            processFiles(item)

    #===============================================================================
    # PARALLEL
    #===============================================================================
    if type==1:
        p = Pool(processes=22)
        pool_outputs =[]

        headers = {
            'Content-Type': 'application/json',
        }

        try: 
            for res in tqdm.tqdm(p.imap_unordered(processFiles, inputFiles), total=len(inputFiles)):
                if len(res) > 0:
                    try:
                        r = requests.post(SOLR_UPDATE, headers=headers, data=json.dumps(res))
                    except Exception as e:
                        logger_deubg.debug('{}\n{}'.format(e, traceback.print_exc()))  
                        fName= SOLR_TMP+str(revolution)                   
                        with open(fName+'.json', 'wb') as outfile:
                            json.dump(res, outfile)
                        pool_outputs = []
                        revolution += 1
            p.close()
            p.join()
                  
            for p in active_children():
               p.terminate()
                           
        except KeyboardInterrupt:
            sys.exit(1)
                  
        #=======================================================================
        # except Exception as e:
        #     print e
        #     print traceback.print_exc()
        #=======================================================================
            
startAnalysis()