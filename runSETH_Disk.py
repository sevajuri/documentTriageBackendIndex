import sys
import cPickle as pickle
from os import listdir, walk, remove, chdir
import os
from os.path import isfile, join, splitext
import traceback
import progressbar
import pubmed_parser as pp
import re
from subprocess import Popen, PIPE
#===============================================================================
# multiprocessing
#===============================================================================
from multiprocessing import Pool, Queue, Process, Manager, current_process, active_children # use threads
try:
    from urllib import urlretrieve
except ImportError: # Python 3
    from urllib.request import urlretrieve
import tqdm

sys.path.insert(0, '../')
from _config import *

print "Preparing Input Files"
mutations_genview = pickle.load(open(PUBTATOR_INFO['MUTATIONS']['gv'], 'rb'))
inputFiles = []
inputFiles.extend([nalaIn+files for files in listdir(nalaIn) if isfile(join(nalaIn, files))])

def getNER_SETH(file):

    foundMutations = {}
    rg = re.compile('(text)=([0-9a-zA-Z_.:>< ]+)')
    
    try:
        text = open(file, 'rb').read().strip()
        pmid = file.rsplit('/',1)[1].split('.')[0]
        chdir(SETH)        
        cmd = 'java -cp seth.jar seth.ner.wrapper.SETHNERAppMut "{}"'.format(text)
        #proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, preexec_fn=os.setsid,close_fds=True)
        proc = Popen(cmd, shell=True, stdout=PIPE, stderr=PIPE, preexec_fn=os.setsid, close_fds=True)
        (output, error) = proc.communicate()
        rere = rg.findall(output)
        muts = list(set([item[1] for item in rere]))
        #print text, foundMutations
        chdir(BACKEND)        
        foundMutations[pmid]=muts

    except Exception, e:
        print e
        traceback.print_exc()

    return foundMutations
                
def runSETH(type=0):
    
    
    bar = progressbar.ProgressBar()
    
    #===============================================================================
    # SINGLE
    #===============================================================================
    if type==1:
        for item in bar(inputFiles):
            res = getNER_SETH(item)
            if res:
                mutations_genview.update(res)
 
    #===============================================================================
    # PARALLEL
    #===============================================================================
    
    if type==0:    
    
        p = Pool(processes=22)
        pool_outputs =[]

        headers = {
            'Content-Type': 'application/json',
        }

        try: 
            for res in tqdm.tqdm(p.imap_unordered(getNER_SETH, inputFiles), total=len(inputFiles)):
                if res:
                    mutations_genview.update(res)

            p.close()
            p.join()
            


            for p in active_children():
               p.terminate()

        except KeyboardInterrupt:
            sys.exit(1)

        except Exception as e:
            print e
            print traceback.print_exc()
            
    pickle.dump(gv_mutations, open(PUBTATOR_INFO['MUTATIONS']['gv'],'wb'), protocol=pickle.HIGHEST_PROTOCOL)

runSETH()