import os
import pkg_resources
import _pickle as pickle
import traceback
import sys
from os import listdir, walk, remove, chdir
from os.path import isfile, join, splitext
import traceback
import progressbar
import pubmed_parser as pp
import re
import gc

#from nalaf.utils.readers import TextFilesReader, PMIDReader
from nalaf.utils.readers import StringReader, TextFilesReader
from nalaf.utils.writers import ConsoleWriter#, TagTogFormat, PubTatorFormat
from nalaf.learning.crfsuite import PyCRFSuite
from nala.utils import PRO_CLASS_ID, MUT_CLASS_ID, PRO_REL_MUT_CLASS_ID, get_prepare_pipeline_for_best_model
#from nalaf.domain.bio.gnormplus import GNormPlusGeneTagger
from nalaf.learning.taggers import StubSameSentenceRelationExtractor
from nala.learning.postprocessing import PostProcessing

#===============================================================================
# multiprocessing
#===============================================================================
from multiprocessing import Pool, Queue, Process, Manager, current_process, active_children # use threads
try:
    from urllib import urlretrieve
except ImportError: # Python 3
    from urllib.request import urlretrieve
import tqdm

sys.path.insert(0, '../')
from _config import *

import warnings
warnings.filterwarnings("ignore")

print("Preparing Input Files")
try:
    mutations_nala = pickle.load(open(PUBTATOR_INFO['MUTATIONS']['nala'], 'rb'), encoding='utf-8')
except IOError:
    mutations_nala = pickle.load(open(PUBTATOR_INFO['MUTATIONS']['gv'], 'rb'), encoding='utf-8')

inputFiles = []
inputFiles.extend([nalaIn+files for files in listdir(nalaIn) if isfile(join(nalaIn, files))])
print("We have {} file to annotate".format(len(inputFiles)))
pipeline = get_prepare_pipeline_for_best_model()
crf = PyCRFSuite()
bin_model = pkg_resources.resource_filename('nala.data', 'default_model')
print('STUFF LOADED')
        
def nalaDisk(file):
    
    articles = getArticle(file)
    for article in articles:
        
        f = open(nalaIn+article['pmid'].strip()+'.txt', 'w')
        text = " ".join( [article['title'], article['abstract'] ] ).strip()

        f.write(text)
        f.close()        

def runNala(file):

    #print('Runing NALA')
    #pipeline = get_prepare_pipeline_for_best_model()
    #crf = PyCRFSuite()
    #bin_model = pkg_resources.resource_filename('nala.data', 'default_model')
    annotationDict = {}

    try:
        #text = open(file, 'r').read().strip()
        pmid = file.rsplit('/',1)[1].split('.')[0]         
        dataset = TextFilesReader(file).read()
        #dataset = StringReader(text).read()
        pipeline.execute(dataset)
        crf.tag(dataset, bin_model, MUT_CLASS_ID)
        PostProcessing().process(dataset)
        x=ConsoleWriter(ent1_class_id=PRO_CLASS_ID, ent2_class_id=MUT_CLASS_ID, color=False).getAnotations(dataset)
        annotationDict[pmid]=x
        gc.collect()
        #=======================================================================
        # print(x)
        # print(annotationDict[pmid])
        #=======================================================================
    except :
        print("Unexpected error:", sys.exc_info()[0])
        raise

    return annotationDict

def runAnnotate(type=1):
    
    bar = progressbar.ProgressBar()
    print('Number of annotated documents:\t{}'.format(len(mutations_nala)))

    #===============================================================================
    # SINGLE
    #===============================================================================
    if type==1:
        for item in inputFiles:
            #print(item)
            res = runNala(item)
            if res:
                mutations_nala.update(res)
 
    #===============================================================================
    # PARALLEL
    #===============================================================================
    
    if type==0:
 
        p = Pool(processes=20)
        pool_outputs =[]
     
        headers = {
            'Content-Type': 'application/json',
        }
     
        try: 
            counter = 0
            for res in tqdm.tqdm(p.imap_unordered(runNala, inputFiles), total=len(inputFiles)):
                #if res:
                mutations_nala.update(res)
                counter += 1
                
                if counter%10000==0:
                    gc.collect()
     
            p.close()
            p.join()
            
            for p in active_children():
               p.terminate()
     
        except KeyboardInterrupt:
            sys.exit(1)
     
        except Exception as e:
            print(e)    
            print(traceback.print_exc())    

    pickle.dump(mutations_nala, open(PUBTATOR_INFO['MUTATIONS']['nala'], 'wb'), 2)
    print('Number of annotated documents:\t{}'.format(len(mutations_nala)))
    
runAnnotate(0)